import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee-form-add',
  templateUrl: 'employee-form-add.component.html',
})
export class EmployeeFormAddComponent implements OnInit {
  date = new Date();

  title = 'Add Form Employee';
  buttonClass = 'bg-blue-500 hover:bg-blue-600 cursor-pointer';
  form: any = {};
  errorForm: any = {};
  statusData: { id: string; name: string }[] = [
    { id: 'non-active', name: 'Non Active' },
    { id: 'active', name: 'Active' },
  ];
  groupData: { id: string; name: string }[] = [
    { id: '1', name: 'Keuangan' },
    { id: '2', name: 'Teknologi' },
    { id: '3', name: 'Sales' },
    { id: '4', name: 'Keamanan' },
    { id: '5', name: 'Kebersihan' },
    { id: '6', name: 'Kebersihan' },
    { id: '7', name: 'Pengendara' },
    { id: '8', name: 'Entertaiment' },
    { id: '9', name: 'Konsumsi' },
    { id: '9', name: 'Luar Negri' },
  ];
  notMatchAccount: any = {
    status: 0,
    message: '',
  };
  ngOnInit(): void {}

  /**
   * submited data
   * @param value 
   * @param valid 
   * @returns 
   */
  handleSubmitData(value: any, valid: any): any {
    if (!this.validationForm()) {
      return false;
    }
    alert('Data Submited');
  }

  /**
   * validation form input login
   * @returns boolean true / false
   */
  validationForm(): boolean {
    if (typeof this.form.username === 'undefined' || !this.form.username || this.form.username === '') {
      return false;
    }
    if (typeof this.form.firstName === 'undefined' || !this.form.firstName || this.form.firstName === '') {
      return false;
    }
    if (typeof this.form.lastName === 'undefined' || !this.form.lastName || this.form.lastName === '') {
      return false;
    }
    if (typeof this.form.email === 'undefined' || !this.form.email || this.form.email === '') {
      return false;
    }
    if (typeof this.form.birthDate === 'undefined' || !this.form.birthDate || this.form.birthDate === '') {
      return false;
    }
    if (typeof this.form.basicSalary === 'undefined' || !this.form.basicSalary || this.form.basicSalary === '') {
      return false;
    }
    if (typeof this.form.status === 'undefined' || !this.form.status || this.form.status === '') {
      return false;
    }
    if (typeof this.form.group === 'undefined' || !this.form.group || this.form.group === '') {
      return false;
    }
    if (typeof this.form.description === 'undefined' || !this.form.description || this.form.description === '') {
      return false;
    }
    return true;
  }

  /**
   * validation form
   * @param obj
   * @returns boolean true/false
   */
  validationFormAutomatically(obj: any): any {
    if (Object.keys(obj).length === 0) {
      return false;
    }
    const filterForm = Object.entries(obj)
      .map(([key, value]) => {
        if (
          typeof this.form[key] === 'undefined' ||
          !this.form[key] ||
          this.form[key] === ''
        ) {
			console.log(key)

          return true;
        } else {
          return false;
        }
      })
      .filter((item) => item);
    return filterForm.length > 0 ? false : true;
  }
}

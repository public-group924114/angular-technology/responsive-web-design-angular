import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.services';
import { Router } from '@angular/router';

@Component({
  templateUrl: 'login.component.html',
})
export class LoginComponent implements OnInit {
  buttonClass = 'bg-blue-500 hover:bg-blue-600 cursor-pointer';
  form: any = {};
  errorForm: any = {};
  notMatchAccount: any = {
    status: 0,
    message: '',
  };
  constructor(public apiService: ApiService, private router: Router) {}
  ngOnInit(): void {
    //   throw new Error('Method not implemented.');
  }

  handleLogin(value: any, valid: any): any {
    if (!this.validationForm()) {
      this.buttonClass = 'bg-gray-500 hover:bg-gray-600 cursor-default';
      return false;
    }
    this.apiService
      .authenticationLogin('assets/data/user.json', this.form)
      .subscribe(
        (data: any) => {
          if (
            this.form.email !== 'user@gmail.com' &&
            this.form.password !== '123'
          ) {
            this.notMatchAccount = {
              status: 1,
              message: 'Username or Password is incorrect',
            };
          } else {
            localStorage.setItem('data', JSON.stringify(data));
            this.router.navigate(['/back/page/employee']);
          }
        },
        (error: any) => {
          alert('Sorry any problem :)');
          console.log('this.error', error.error.error);
        }
      );
  }

  /**
   * validation form input login
   * @returns boolean true / false
   */
  validationForm(): boolean {
    if (
      typeof this.form.password === 'undefined' ||
      !this.form.password ||
      this.form.password === ''
    ) {
      return false;
    }
    return true;
  }
}

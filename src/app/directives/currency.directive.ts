import { DecimalPipe } from '@angular/common';
import { Directive, HostListener, ElementRef, OnInit } from '@angular/core';

@Directive({ selector: '[appEsPriceFormat]' })
export class EsPriceFormatDirective implements OnInit {

    private el: any;
    private PREFIX: string;
    private DECIMAL_SEPARATOR: string;
    private THOUSANDS_SEPARATOR: string;
    private SUFFIX: string;

    constructor(
        private elementRef: ElementRef,
        private decimalPipe: DecimalPipe
    ) {
        this.el = this.elementRef.nativeElement;
        this.PREFIX = '';
        this.DECIMAL_SEPARATOR = '.';
        this.THOUSANDS_SEPARATOR = ',';
        this.SUFFIX = '';
    }

    ngOnInit() {
        setTimeout(() => {
            if (!isNaN(this.el.value)) {
                this.el.value = this.decimalPipe.transform(this.el.value);
            }
        }, 200);
    }

    @HostListener('focus', ['$event.target.value'])
    onFocus(value: any) {
        let [ integer, fraction = '' ] = (value || '').replace(this.PREFIX, '')
                                                  .replace(this.SUFFIX, '')
                                                  .split(this.DECIMAL_SEPARATOR);

        const fractionSize = 2;
        const PADDING = '000000';

        integer = integer.replace(new RegExp(this.THOUSANDS_SEPARATOR, 'g'), '');

        fraction = parseInt(fraction, 10) > 0 && fractionSize > 0
        ? this.DECIMAL_SEPARATOR + (fraction + PADDING).substring(0, fractionSize)
        : '';

        this.el.value = integer + fraction;
    }

    @HostListener('blur', ['$event.target.value'])
    onBlur(value: number) {
        if (!isNaN(value)) {
            this.el.value = this.decimalPipe.transform(value);
        }
    }

}

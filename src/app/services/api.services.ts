import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable()
export class ApiService {
  protected endPoint: string | undefined;
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    observe: 'response' as 'response',
  };

  constructor(private http: HttpClient, private router: Router) {}

  setHeader() {
    let key = '';
    return new HttpHeaders({
      Authorization: key,
      'Content-Type': 'application/json',
    });
  }

  get(endpoint: string, params: any) {
    return this.http.get<any>(environment.apiUrl + endpoint, {
      headers: this.setHeader(),
      params: params,
    });
  }

  post(endpoint: string, data: any, withHeader: boolean = true) {
    let headers = withHeader ? this.setHeader() : {};
    return this.http
      .post<any>(environment.apiUrl + endpoint, data, { headers: headers })
      .pipe(
        map((response) => {
          return response;
        })
      );
  }

  put(endpoint: string, data: any) {
    return this.http
      .put<any>(environment.apiUrl + endpoint, data, {
        headers: this.setHeader(),
      })
      .pipe(
        map((response) => {
          return response;
        })
      );
  }

  delete(endpoint: string, id: string) {
    return this.http
      .delete<any>(environment.apiUrl + endpoint + '/' + id, {
        headers: this.setHeader(),
      })
      .pipe(
        map((response) => {
          return response;
        })
      );
  }

  authenticationLogin(endpoint: string, data: any, withHeader: boolean = true) {
    let headers = withHeader ? this.setHeader() : {};
    return this.http
      .post<any>(environment.apiUrl + endpoint, data, { headers: headers })
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }
}

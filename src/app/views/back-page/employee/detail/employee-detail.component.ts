import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../services/api.services';
import { CurrencyPipe, DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';

@Component({
  selector: 'app-employee-detail',
  templateUrl: 'employee-detail.component.html',
  providers: [DatePipe, CurrencyPipe, {provide: LOCALE_ID, useValue: 'en-ID' }],
})
export class EmployeeDetailComponent implements OnInit {
  date = new Date();
  data: any = {};
  title = 'Detail Employee';

  constructor(public apiService: ApiService, private datePipe: DatePipe, private currencyPipe: CurrencyPipe) {}

  ngOnInit(): void {
    this.handleGetData();
  }

  /**
   * get data using fetch
   */
  handleGetData() {
    this.apiService
      .get('assets/data/edit-employee.json', {})
      .subscribe((res: any) => {
        // this.form.
        this.data = res;
        this.data.birthDate = this.datePipe.transform(res.birthDate,'yyyy-MM-dd');
        this.data.basicSalary = Number(res.basicSalary.replace(/\./g, ''));
      });
  }
}

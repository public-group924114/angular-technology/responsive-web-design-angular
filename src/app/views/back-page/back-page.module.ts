import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { DashboardComponent } from './dashboard/dashboard.component';
import { BackPageRoutingModule } from './back-page-routing.module';
import { EmployeeComponent } from './employee/employee.component';
import { EmployeeFormAddComponent } from './employee/form-add/employee-form-add.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule, DecimalPipe } from '@angular/common';
import { EmployeeFormEditComponent } from './employee/form-edit/employee-form-edit.component';
import { EsPriceFormatDirective } from '../../directives/currency.directive';
import { ApiService } from '../../services/api.services';
import { EmployeeDetailComponent } from './employee/detail/employee-detail.component';

@NgModule({
  imports: [FormsModule, BackPageRoutingModule, NgSelectModule, CommonModule],
  declarations: [
    DashboardComponent,
    EmployeeComponent,
    EmployeeFormAddComponent,
    EmployeeFormEditComponent,
    EmployeeDetailComponent,
    EsPriceFormatDirective,
  ],
  exports: [EsPriceFormatDirective],
  providers: [ApiService, DecimalPipe],
})
export class BackPageModule {}

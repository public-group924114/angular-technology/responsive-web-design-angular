import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { DashboardComponent } from './dashboard/dashboard.component';
import { FrontPageRoutingModule } from './front-page-routing.module';
import { LoginComponent } from './login/login.component';
import { CommonModule } from '@angular/common';
import { ApiService } from '../../services/api.services';

@NgModule({
  imports: [FormsModule, FrontPageRoutingModule, CommonModule],
  declarations: [DashboardComponent, LoginComponent],
  providers: [ApiService],
})
export class FrontPageModule {}

# Responsive Angular App

Welcome to Responsive Angular Apps! This project is built using Angular and focuses on creating a responsive web application that adapts to various screen sizes and devices. This application has the features of search list data, login, create data, edit data, delete data and you can deployed using docker

## Requirements

To set up and run this project, you need to have the following installed:

- Node.js v18.19.0
- Npm 10.2.3
- Angular CLI: 17.2.3

## Installation

1. **Clone the repository:**

    ```bash
    git clone https://github.com/ivannofick/responsive-web-design-angular.git
    ```

2. **Navigate to the project directory:**

    ```bash
    cd responsive-web-design-angular
    ```

3. **Install dependencies:**

    ```bash
    npm install
    ```

## Development on local

Run `ng serve --open` for a development local. The app will automatically reload if you change any of the source files.

```bash
ng serve --open
```
or
```bash
npm start
```

## Account Accessed

If the user does not fill in the account by logging in with `user@gmail.com` then you cannot log in

```bash
-u = user@gmail.com
-p = 123
```

## Search Employee

If you want to search within the list of employees, please enter `jansen` in the search form.

```bash
jansen
```
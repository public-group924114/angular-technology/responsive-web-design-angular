import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { EmployeeComponent } from './employee/employee.component';
import { EmployeeFormAddComponent } from './employee/form-add/employee-form-add.component';
import { EmployeeFormEditComponent } from './employee/form-edit/employee-form-edit.component';
import { EmployeeDetailComponent } from './employee/detail/employee-detail.component';

const routes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        data: {
            title: 'Dashboard',
        },
    },
    {
        path: 'employee',
        component: EmployeeComponent,
        data: {
            title: 'Employee',
        }
    },
    {
        path: 'employee/form/add',
        component: EmployeeFormAddComponent,
        data: {
            title: 'Employee Form',
        },
    },
    {
        path: 'employee/form/edit/:id',
        component: EmployeeFormEditComponent,
        data: {
            title: 'Employee Form',
        },
    },
    {
        path: 'employee/detail/:id',
        component: EmployeeDetailComponent,
        data: {
            title: 'Employee Detail',
        },
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BackPageRoutingModule {}

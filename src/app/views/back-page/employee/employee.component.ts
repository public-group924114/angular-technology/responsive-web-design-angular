import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.services';
import { CurrencyPipe, DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
@Component({
  templateUrl: 'employee.component.html',
  providers: [
    DatePipe,
    CurrencyPipe,
    { provide: LOCALE_ID, useValue: 'en-ID' },
  ],
})
export class EmployeeComponent implements OnInit {
  title = 'Data Employee';
  data: any = [];
  formSearch: any = {};

  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    this.handleGetData();
  }

  /**
   * get data using fetch
   */
  handleGetData() {
    this.apiService
      .get('assets/data/all-employee.json', {})
      .subscribe((res: any) => {
        this.data = res;
      });
  }

  /**
   * Filter Data List
   */
  doSearch(value: any, valid: any) {
    if (this.formSearch.search) {
      this.data = this.data.filter((row: any) => {
        return this.formSearch.search === row.username;
      });
    }
  }

  /**
   * Filter Data List
   */
  resetSearch(event: any): void {
    this.handleGetData();
  }
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { FrontPageLayoutComponent } from './components';
import { AppRoutingModule } from './app.routing';
import { AuthGuard } from './services/auth.guard';
import { BackPageLayoutComponent } from './components/back-page-layout';

@NgModule({
  declarations: [AppComponent, FrontPageLayoutComponent, BackPageLayoutComponent],
  imports: [BrowserModule, FormsModule, HttpClientModule, AppRoutingModule, BrowserModule],
  providers: [AuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}

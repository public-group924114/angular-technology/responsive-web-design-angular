import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanActivate } from '@angular/router';
import { FrontPageLayoutComponent } from './components';
import { AuthGuard } from './services/auth.guard';
import { BackPageLayoutComponent } from './components/back-page-layout';
// Import Containers



export const routes: Routes = [
  {
      path: '',
      redirectTo: 'pages/login',
      pathMatch: 'full',
  },
  {
    path: '',
    component: FrontPageLayoutComponent,
    data: {
        title: 'Home'
    },
    children: [
      {
          path: 'pages',
          loadChildren: () => import('./views/front-page/front-page.module').then(m => m.FrontPageModule)
      }
    ]
  },
  {
    path: 'back/page',
    component: BackPageLayoutComponent,
    data: {
        title: 'Home Back Page'
    },
    // canActivate: [AuthGuard],
    children: [
      {
          path: '',
          loadChildren: () => import('./views/back-page/back-page.module').then(m => m.BackPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'}) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
